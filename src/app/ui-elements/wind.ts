import * as d3 from 'd3';
import { FrameDTO } from "../models/dtos/frame-dto";
import { UIElement } from "./ui-element";

export class WindGauge implements UIElement {
    private svg: any

    constructor(parentID: string) {
        let parent = document.getElementById(parentID)
        this.svg = d3.select(parent).append('svg')
    }

    update(frame: FrameDTO) {
        const margin = 10;
        const max_wind = 20.0

        let parent = this.svg.node().parentNode

        var width = parent.clientWidth
        var height = parent.clientHeight

        // Viewport update
        this.svg
            .attr("width", width)
            .attr("height", height)
            .attr('viewBox', [0, 0, width, height])

        let x = (width - 2 * margin) / 2
        let y = (height - 2 * margin) / 2
        let r = Math.min(x, y)

        this.svg.selectAll('.df_wind_circle').data([
            { x: x, y: y, r: r }
        ])
            .enter()
            .append('circle')
            .attr('class', 'df_wind_circle')
            .attr('cx', d => d.x)
            .attr('cy', d => d.y)
            .attr('r', d => d.r)
            .attr('stroke-width', 1)
            .attr('stroke', 'black')
            .attr('fill', 'white')
            .attr('transform', `translate(${margin}, ${margin})`)

        this.svg.selectAll('.df_wind_dir').data([
            { dir: frame.wind_direction }
        ])
            .enter()
            .append('text')
            .attr('class', 'df_wind_dir')
            .attr('transform', `translate(${x + margin}, ${y + margin - 15})`)
            .style('font-size', '8pt')
            .style('text-anchor', 'middle')

        this.svg.selectAll('.df_wind_speed').data([
            { dir: frame.wind_direction }
        ])
            .enter()
            .append('text')
            .attr('class', 'df_wind_speed')
            .attr('transform', `translate(${x + margin}, ${y + margin + 20})`)
            .style('font-size', '8pt')
            .style('text-anchor', 'middle')

        this.svg.selectAll('.df_wind_arrow').data([
            { direction: frame.wind_direction, speed: frame.wind_speed }
        ])
            .enter()
            .append('line')
            .attr('class', 'df_wind_arrow')
            .attr('x1', 0)
            .attr('y1', 0)
            .attr('x2', 0)
            .attr('stroke-width', 3)
            .attr('stroke', 'red')

        this.svg.selectAll('.df_wind_arrow')
            .transition()
            .duration(100)
            .attr('y2', frame.wind_speed * (-r / max_wind))
            .attr('transform', `translate(${x + margin}, ${y + margin}) rotate(${frame.wind_direction + 180})`)

        this.svg.selectAll('.df_wind_dir')
            .transition()
            .duration(100)
            .text(`${frame.wind_direction.toFixed(2)}°`)

        this.svg.selectAll('.df_wind_speed')
            .transition()
            .duration(100)
            .text(`${frame.wind_speed.toFixed(2)} m/s`)
        
        // Label
        this.svg.selectAll('.df_temp_text').data([null])
            .enter()
            .append('text')
            .attr('class', 'df_temp_text')
            .style('font-size', '10pt')
            .style('text-anchor', 'middle')
            .attr('transform', `translate(${width / 2}, ${height - 10})`)
            .text('Wind')
    }
}