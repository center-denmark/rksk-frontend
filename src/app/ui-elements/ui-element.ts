import { FrameDTO } from "../models/dtos/frame-dto";

export interface UIElement {
    update(frame: FrameDTO)
}