import * as d3 from 'd3';
import { Color } from '../models/color';
import { FrameDTO } from "../models/dtos/frame-dto";
import { Utils } from '../utils';
import { UIElement } from "./ui-element";

export class VerticalBarCO2 implements UIElement {
    private svg: any

    constructor(parentID: string) {
        let parent = document.getElementById(parentID)
        this.svg = d3.select(parent).append('svg')
    }

    update(frame: FrameDTO) {
        const margin = 10;
        const max_co2 = 205

        let parent = this.svg.node().parentNode

        var width = parent.clientWidth
        var height = parent.clientHeight

        // Viewport update
        this.svg
          .attr("width", width)
          .attr("height", height)
          .attr('viewBox', [0, 0, width, height])

        // Label
        this.svg.selectAll('.df_co2_text').data([null])
          .enter()
          .append('text')
          .attr('class', 'df_co2_text')
          .style('font-size', '10pt')
          .style('text-anchor', 'middle')
          .attr('transform', `translate(${width / 2}, ${height - 10})`)
          .text('CO2')

          // Value
          this.svg.selectAll('.df_co2_val').data([null])
            .enter()
            .append('text')
            .attr('class', 'df_co2_val')
            .attr('transform', `translate(${width / 2}, ${margin + 5})`)
            .style('font-size', '8pt')
            .style('text-anchor', 'middle')

          this.svg.selectAll('.df_co2_val')
            .transition()
            .duration(100)
            .text(`${frame.co2.toFixed(2)} g/kWh`)

          // Rect
          const offset_top = 20
          const offset_bottom = 23
          
          const color_from = new Color(0, 255, 0)
          const color_to = new Color(0, 0, 0)
          
          const scale = d3.scaleLinear()
            .domain([0, max_co2]) // Arbitrary number...change!
            .range([0, height - offset_top - offset_bottom])

          const percentage = frame.co2 / scale.domain()[1]
          const color = Utils.interpolate(color_from, color_to, percentage)

          this.svg.selectAll('.df_co2_rect').data([null])
            .enter()
            .append('rect')
            .attr('class', 'df_co2_rect')
            .attr('x', -15)
            .attr('width', 30)

          this.svg.selectAll('.df_co2_rect')
            .transition()
            .duration(100)
            .attr('height', scale(frame.co2))
            .attr('transform', `translate(${width / 2}, ${height - offset_bottom - scale(frame.co2)})`)  
            .attr('fill', `rgb(${color.r}, ${color.g}, ${color.b})`)
      }
}