import * as d3 from 'd3';
import { FrameDTO } from '../models/dtos/frame-dto';
import { UIElement } from './ui-element';

export class Timeline implements UIElement {
    private timeline_svg: any
    private timeline_scale: any
    private timeline_axis: any

    constructor(parentID: string) {
        let parent = document.getElementById(parentID)

        this.timeline_svg = d3.select(parent).append('svg')

        this.timeline_scale = d3.scaleTime()
    
        this.timeline_axis = d3.axisBottom(this.timeline_scale)
          .tickFormat(d3.timeFormat("%H:%M %a %d. %b"))
          .ticks(d3.timeHour.every(6))
    }

    update(frame: FrameDTO) {
        let margin = 20;
    
        let parent = this.timeline_svg.node().parentNode

        var width = parent.clientWidth
        var height = parent.clientHeight
    
        this.timeline_svg
          .attr("width", width)
          .attr("height", height)
          .attr('viewBox', [0, 0, width, height])
          
        let timestamp = frame.timestamp
        let timespan = 24 * 3600 * 1000
    
        /* Timeline */
        this.timeline_scale
            .domain([timestamp - timespan, timestamp + timespan])
            .range([0, width - margin * 2]) 
    
        var axisGroup = this.timeline_svg.selectAll('.axis').data([null])
    
        axisGroup = axisGroup
          .enter().append('g')
          .attr('class', 'axis')
          .style('font-size', '10pt')
          .style('font-weight', 'bold')
          .merge(axisGroup)
          .attr('transform', `translate(${margin}, ${margin})`)
          .call(this.timeline_axis)
    
    
        /* Current time marker */
        var lines = this.timeline_svg.selectAll('.x-marker').data([
          {x1: this.timeline_scale(timestamp), y1: 0, x2: this.timeline_scale(timestamp), y2: height}
        ])
    
        lines.enter().append('line')
                .attr('class', 'x-marker')
                .style('stroke-width', 2)
                .style('stroke', 'black')
                .attr('transform', `translate(${margin}, 0)`)
            .merge(lines)
                .attr('x1', (d) => { return d.x1 })
                .attr('y1', (d) => { return d.y1 })
                .attr('x2', (d) => { return d.x2 })
                .attr('y2', (d) => { return d.y2 })
      }
}