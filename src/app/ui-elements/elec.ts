import * as d3 from 'd3';
import { FrameDTO } from "../models/dtos/frame-dto";
import { UIElement } from "./ui-element";

export class Electricity implements UIElement {
    private svg: any

    constructor(parentID: string) {
        let parent = document.getElementById(parentID)
        this.svg = d3.select(parent).append('svg')
    }

    update(frame: FrameDTO) {
        var data = [
            { label: ['Production'], value: frame.production, cond_label: false, cond_col: false },
            { label: ['Consumption'], value: frame.consumption, cond_label: false, cond_col: false },
            { label: ['Import', 'Export'], value: frame.production - frame.consumption, cond_label: true, cond_col: true },
        ]

        // const max = Math.max.apply(null, data.map(d => d.value))
        const max = 60000

        const margin = 10;
        const base_offset = 100;

        let parent = this.svg.node().parentNode

        var width = parent.clientWidth
        var height = parent.clientHeight

        // Viewport update
        this.svg
            .attr("width", width)
            .attr("height", height)
            .attr('viewBox', [0, 0, width, height])

        // Scaling update
        const xScale = d3.scaleLinear()
            .domain([0, max]) // Arbitrary number...change!
            .range([0, width - 220])

        const yScale = d3.scaleBand()
            .domain(data.map(d => d.label))
            .range([0, height - 2 * margin])
            .padding(0.1)

        const color = (val: number, conditional_color: boolean) => {
            if (!conditional_color) return 'blue'

            return (val > 0) ? 'green' : 'red'
        }

        const label = (label: string, val: number, conditional_label: boolean) => {
            if (!conditional_label) return label[0]

            return (val < 0) ? label[0] : label[1]
        }

        // Add elements
        this.svg.selectAll('.df-elec-rect').data(data)
            .enter()
            .append('rect')
            .attr('class', 'df-elec-rect')
            .attr('transform', `translate(${margin * 2 + base_offset}, ${margin})`)

        this.svg.selectAll('.df-elec-val').data(data)
            .enter()
            .append('text')
            .attr('class', 'df-elec-val')
            .attr('transform', `translate(${margin * 2 + base_offset}, ${margin})`)

        this.svg.selectAll('.df_elec_label').data(data)
            .enter()
            .append('text')
            .attr('class', 'df_elec_label')
            .attr('transform', `translate(${margin}, ${margin})`)
            .attr('y', d => yScale(d.label) + yScale.bandwidth() / 2 + 5)

        // Transitions
        this.svg.selectAll('.df-elec-rect')
            .transition()
            .duration(100)
            .attr('y', d => yScale(d.label))
            .attr('width', d => xScale(Math.abs(d.value)))
            .attr('height', yScale.bandwidth())
            .attr('fill', d => color(d.value, d.cond_col))

        this.svg.selectAll('.df-elec-val')
            .transition()
            .duration(100)
            .attr('y', d => yScale(d.label) + yScale.bandwidth() / 2 + 5)
            .attr('x', d => xScale(Math.abs(d.value)) + 5)
            .text(d => `${Math.abs(d.value.toFixed(2))} kW`)

        this.svg.selectAll('.df_elec_label')
            .transition()
            .duration(100)
            .text(d => label(d.label, d.value, d.cond_label))
            .attr('fill', d => color(d.value, d.cond_col))
    }
}