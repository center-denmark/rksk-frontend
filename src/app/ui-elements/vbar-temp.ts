import * as d3 from 'd3';
import { Color } from '../models/color';
import { FrameDTO } from "../models/dtos/frame-dto";
import { Utils } from '../utils';
import { UIElement } from "./ui-element";

export class VerticalBarTemperature implements UIElement {
    private svg: any

    constructor(parentID: string) {
        let parent = document.getElementById(parentID)
        this.svg = d3.select(parent).append('svg')
    }

    update(frame: FrameDTO) {
        const margin = 10;
        const max_temp = 19

        let parent = this.svg.node().parentNode

        var width = parent.clientWidth
        var height = parent.clientHeight

        // Viewport update
        this.svg
            .attr("width", width)
            .attr("height", height)
            .attr('viewBox', [0, 0, width, height])

        // Label
        this.svg.selectAll('.df_temp_text').data([null])
            .enter()
            .append('text')
            .attr('class', 'df_temp_text')
            .style('font-size', '10pt')
            .style('text-anchor', 'middle')
            .attr('transform', `translate(${width / 2}, ${height - 10})`)
            .text('Temperature')

        // Value
        this.svg.selectAll('.df_temp_val').data([null])
            .enter()
            .append('text')
            .attr('class', 'df_temp_val')
            .attr('transform', `translate(${width / 2}, ${margin + 5})`)
            .style('font-size', '8pt')
            .style('text-anchor', 'middle')

        this.svg.selectAll('.df_temp_val')
            .transition()
            .duration(100)
            .text(`${frame.temp.toFixed(2)} C°`)

        // Rect
        const offset_top = 20
        const offset_bottom = 23

        const color_from = new Color(0, 0, 255)
        const color_to = new Color(255, 0, 0)

        const scale = d3.scaleLinear()
            .domain([0, max_temp]) // Arbitrary number...change!
            .range([0, height - offset_top - offset_bottom])

        let percentage = frame.temp / scale.domain()[1]
        let color = Utils.interpolate(color_from, color_to, percentage)

        this.svg.selectAll('.df_temp_rect').data([null])
            .enter()
            .append('rect')
            .attr('class', 'df_temp_rect')
            .attr('x', -15)
            .attr('width', 30)
            .attr('fill', 'gray')

        this.svg.selectAll('.df_temp_rect')
            .transition()
            .duration(100)
            .attr('height', scale(frame.temp))
            .attr('transform', `translate(${width / 2}, ${height - offset_bottom - scale(frame.temp)})`)
            .attr('fill', `rgb(${color.r}, ${color.g}, ${color.b})`)
    }
}