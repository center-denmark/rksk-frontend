export enum IconColor {
    Blue,
    Gold,
    Red,
    Green,
    Orange,
    Yellow,
    Violet,
    Grey,
    Black
  }