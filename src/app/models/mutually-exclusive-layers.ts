import { Layer } from "./layer";
import { Layers } from "./layers";

export class MutuallyExclusiveLayers extends Layers {
    add(layer: Layer) {
        layer.setEnabled(this.layers.length == 0)
        super.add(layer)
    }

    toggleLayer(layer: Layer) {
        if (this.layers.length > 0) {
            this.layers.forEach((l, i) => {
                l.setEnabled(l.getID() == layer.getID())
            })
        }

        this.updateVisible()
    }
}