import { BehaviorSubject, Observable, of, Subject } from "rxjs";
import { Layer } from "./layer";
import * as L from 'leaflet';

export class Layers {
    private parentLayer: L.FeatureGroup = new L.FeatureGroup()

    protected layers: Layer[] = []
    protected layersSubject = new BehaviorSubject<Layer[]>(this.layers)

    clear() {
        this.layers = []
        this.layersSubject.next(this.layers)
    }

    add(layer: Layer) {
        this.layers.push(layer)
        this.layersSubject.next(this.layers)
        this.updateVisible()
    }
    
    isNoneShowing(): boolean {
        return this.parentLayer.getLayers().length == 0
    }

    getLayers(): Observable<Layer[]> {
        return this.layersSubject
    }

    toggleLayer(layer: Layer) {
        layer.toggle()
        this.updateVisible()
    }

    getParentLayer(): L.Layer {
        return this.parentLayer
    }

    getBounds(): L.LatLngBounds {
        return this.parentLayer.getBounds()
    }

    protected updateVisible() {
        this.layers.forEach(layer => {
            if (layer.getEnabled()) {
                this.parentLayer.addLayer(layer.getLayer())
            } else {
                this.parentLayer.removeLayer(layer.getLayer())                
            }
        })
    }
}