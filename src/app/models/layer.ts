import { LayerDTO } from "./dtos/layer-dto";
import * as L from 'leaflet';
import 'leaflet.markercluster'

export class Layer {
    private id: string
    private name: string
    private icon: string
    private enabled: boolean

    private layer: L.FeatureGroup
    private layer2: any

    constructor(layer: LayerDTO) {
        this.id = layer.id
        this.name = layer.name
        this.icon = layer.icon
        this.enabled = (layer.enabled == true) // "Hacky" typecasting from DB (0 or 1)

        this.layer = L.featureGroup()

        if (layer.clustered) {
            this.layer2 = L.markerClusterGroup({

                // Insert custom cluster icon here!
                iconCreateFunction: function (cluster) {

                    let markers = cluster.getAllChildMarkers()
                    let icon = markers[0].options.icon as L.DivIcon
           
                    let _view = document.createElement('div')
                    // _view.style.border = '1px dashed black'
                    _view.style.borderRadius = '25px'
                    _view.style.padding = '0px'
                    _view.style.textAlign = 'center'
           
                    let html = icon.options.html

                    if (html instanceof HTMLElement) {
                        let clone = html.cloneNode(true)
                        _view.appendChild(clone)
                    }
                    
                    let lbl = document.createElement('span')
                    lbl.style.fontSize = '10pt'
                    lbl.style.fontWeight = 'bold'
                    lbl.innerHTML = '' + markers.length
                    
                    _view.appendChild(lbl)         
           
                    return L.divIcon({
                       className: "leaflet-data-marker",
                       html: _view,
                       iconSize: icon.options.iconSize,
                       iconAnchor: icon.options.iconAnchor
                    })
                 },

                zoomToBoundsOnClick: true,
                showCoverageOnHover: false
            })

            this.layer2.addTo(this.layer)
        } else {
            this.layer2 = this.layer
        }
    }
    
    getID(): string {
        return this.id
    }

    setEnabled(enabled: boolean) {
        this.enabled = enabled
    }

    getEnabled(): boolean {
        return this.enabled
    }

    add(layer: L.Layer) {
        layer.addTo(this.layer2)
    }

    getLayer(): L.Layer {
        return this.layer
    }

    toggle() {        
        this.setEnabled(!this.enabled)
    }
}