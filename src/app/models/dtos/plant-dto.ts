export interface PlantDTO {
    year: number,
    company_id: number,
    plant_id: number,
    company_name: string,
    plant_name: string,
    plant_type_name: string,
    zipcode: number,
    municipality_name: string,
    municipality_no: number,
    main_fuel: string,
    latitude: number,
    longitude: number,
    capacity_elec_min_kW: number,
    capacity_elec_max_kW: number,
    capacity_heat_min_kW: number,
    capacity_heat_max_kW: number
}