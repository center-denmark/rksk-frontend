
export interface LayerDTO {
    id: string
    name: string
    icon: string
    enabled: boolean
    clustered: boolean
}