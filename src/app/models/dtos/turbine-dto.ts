export interface TurbineDTO {
    gsrn: string,
    connection_date: string,
    capacity_kW: number,
    rotor_diameter_m: number,
    hub_height_m: number,
    manufacturer: string,
    model: string,
    municipality_no: number,
    municipality: string,
    location_type: string,
    owner: string,
    cadastre_no: string,
    grid_company_inst_no: string,
    latitude: number,
    longitude: number
}