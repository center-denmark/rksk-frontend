export interface FrameDTO {
    num: number,
    consumption: number,
    production: number,
    timestamp: number,
    temp: number,
    wind_speed: number,
    wind_direction: number,
    co2: number
}