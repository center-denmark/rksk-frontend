import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PlantDTO } from 'src/app/models/dtos/plant-dto';

@Injectable({
  providedIn: 'root'
})
export class PlantsService {

  constructor(private http: HttpClient) { }

  getPlants(municipality_no: number): Observable<PlantDTO[]> {
    return this.http.get<PlantDTO[]>('/api/v1/plants/' + municipality_no)
  }
}
