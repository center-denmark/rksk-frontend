import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { io } from 'socket.io-client'
import { FrameDTO } from 'src/app/models/dtos/frame-dto';

@Injectable({
  providedIn: 'root'
})
export class DatafeedService {
  private socket

  private datafeed: Subject<any>

  constructor() {
    this.socket = io()
  }

  getFrames(): Observable<FrameDTO> {
  
    let observable = new Observable<FrameDTO>(observer => {
      this.socket.on('frame', (frame) => {
        observer.next(frame)
      })
    })

    return observable
  }
}
