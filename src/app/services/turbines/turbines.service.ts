import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TurbineDTO } from 'src/app/models/dtos/turbine-dto';

@Injectable({
  providedIn: 'root'
})
export class TurbinesService {

  constructor(private http: HttpClient) { }

  getTurbines(municipality_no: number): Observable<TurbineDTO[]> {
    return this.http.get<TurbineDTO[]>('/api/v1/windturbines/' + municipality_no)
  }
}
