import { Color } from "./models/color";

export class Utils {
    static interpolate(c1: Color, c2: Color, percentage: number): Color {

        let r = c1.r + (c2.r - c1.r) * percentage
        let g = c1.g + (c2.g - c1.g) * percentage
        let b = c1.b + (c2.b - c1.b) * percentage

        let color = new Color(r, g, b)

        return color
    }
}